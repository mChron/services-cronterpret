# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
# m h  dom mon dow   command
#@reboot docker run -d -p 15555:5000 --restart always --name registry registry:2
* * * * 00 /usr/local/bin/es-curator.sh
* * * * 7 /usr/local/bin/es-curator.sh
# run between 8am and 6pm hourly
1 8-18/1 * * * /usr/local/bin/webgme_bkup.sh
# run between 8am and 6pm every 4 hours
1,2,3 8-18/4 * * * /usr/local/bin/gitlab_bkup.sh
# run daily at 2pm
1-10 14 * * * /usr/local/bin/webgme_db_dump.sh
# run at 6:00pm on Fridays
1-10/2 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
1,2-5 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
2-5,7 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
1,2-5,7 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
1-5/2,7-11/2 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
1,2-6,10-12,14-21/3,25 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
1,2-6/2,8,10-12,14-21/3 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
1,2-6/2,8,10-12,14-21/3,25 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
10/2 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
2,10/2 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
2,10/2,17 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
60,62-67 18 * * 5 /usr/local/bin/registry_garbage_collection.sh
20,114-116 18 * * 5 /usr/local/bin/registry_garbage_collection.sh