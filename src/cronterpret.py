# read in file line by line
# parse crontab entry
# min hour DoM mon DoW Command
# accepted formats for each field
# Simple numeral ##, execute at this time
# Comma separated list = ##,##
# Range with interval = ##-##/##, between 8 and 16 every 4 = 8-16/4
import os
from os import path
import re

# don't accept interval expression after a non interval, e.g. 10/2 or 2,10/2 or 2,10/2,17
# don't accept numbers higher than 59
# don't accept numbers 3 digits or more
# any, single digit or comma delimited list
# range with comma delimited on either side (1,3-5,7) or (1,3-9/3,11)
# range (1-3), range with interval (1-6/2),
# ranges comma delimited (1-5,6-10) or (1-5/2,6-10/3)
# ranges comma delimited with single digit between (1-4,5,6-10) or (1-4,5,6-10,11)
acceptable = "(\*|(\d{1,2},?)+$|(\d{1,2},)*(\d{1,2}-\d{1,2}(/\d{1,2})?)(,\d{1,2}-\d{1,2}(/\d{1,2})?)*((,\d{1,2}-\d{1,2}(/\d{1,2})?)|,\d{1,2})*|\d{1,2}-\d{1,2}(/\d{1,2})?)"
invalid_exp = ["^(\d{1,2}(,\d{1,2})?)/\d{1,2}(,\d{1,2})?$"]
restrict_min = ["\d{3,}", "[6-9][0-9]"]
restrict_hour = ["\d{3,}", "2[4-9]", "[3-9][0-9]"]
restrict_dom = ["\d{3,}", "0[0]?$", "3[2-9]", "[4-9][0-9]"]
restrict_mon = ["\d{3,}", "0[0]?$", "[1-9][3-9]"]
restrict_dow = ["\d{3,}", "[7-9]", "[1-9][0-9]"]

def get_ordinal_suffix(i):
    n = int(i)
    suffix = ['th', 'st', 'nd', 'rd', 'th'][min(n % 10, 4)]
    if 11 <= (n % 100) <= 13:
        suffix = 'th'
    return str(n) + suffix

def format_minute(m):
    if m and m != "*":
        return "the " + get_ordinal_suffix(m)
    elif m == "*":
        return "every"

def format_message(m,h,dm,mo,dw,command):
    m = format_minute(m)
    if m and h and dm and mo and dw:
        print("{command} will be run at {m} minute".format(command=command, m=m))

def validate_time_component(component, additional_restrictions, component_name):
    invalid = []
    invalid.extend(invalid_exp)
    invalid.extend(additional_restrictions)
    m = re.search(acceptable, component)
    valid = True
    for wrong in invalid:
        w = re.search(wrong, component)
        if w:
            print("Invalid {component_name} expression".format(component_name=component_name), w.group(0))
            return False
    if m:
        return m.group(0)

def parse_line(line, next, last):
    print("Processing: {line}".format(line=line))
    ops = line.split(" ")
    min = ops[0]
    hour = ops[1]
    dom = ops[2]
    mon = ops[3]
    dow = ops[4]
    command = ops[5]
    m = validate_time_component(min, restrict_min, "Minute")
    h = validate_time_component(hour, restrict_hour, "Hour")
    dm = validate_time_component(dom, restrict_dom, "Day Of Month")
    mo = validate_time_component(mon, restrict_mon, "Month")
    dw = validate_time_component(dow, restrict_dow, "Day Of Week")
    format_message(m,h,dm,mo,dw,command)

def parse_tab(tab, next, last):
    try:
        with open(tab) as f:
            for line in f:
                if line[0] != "#":
                    parse_line(line, next, last)
    except IOError:
        print("File '{tab}' not accessible.".format(tab=tab))

def interpret_cronfile(file, next, last):
    if next and last:
        print("Please use --next or --last, not both.")
        return
    if file != "" and not path.exists(file):
        print("File does not exist")
        return
    tab = ""
    if file == "":
        stream = os.popen('crontab -l')
        tab = stream.read()
        file = ".user_crontab"
        try :
            with open(file, "w") as f:
                f.write(tab)
        except IOError:
            print("Failed to write user crontab to temporary file")
    parse_tab(file, next, last)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Interpret cron expressions from a file or your user crontab.")
    parser.add_argument('--file', '-f', help='the file you want to interpret.', default="")
    parser.add_argument('--next', '-n', help='determine the next time a cronjob will run', action='store_true')
    parser.add_argument('--last', '-l', help='determine the last time a cronjob should have run', action='store_true')

    args = parser.parse_args()

    interpret_cronfile(args.file, args.next, args.last)